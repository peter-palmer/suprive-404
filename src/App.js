import React, { useEffect } from 'react';
import Router from './Router';

// SASS Importing
import './App.scss';

// Libraries
import AOS from 'aos';
import SmoothScroll from 'smooth-scroll';

function App() {
	useEffect(() => {
		AOS.init();
		new SmoothScroll('a[href*="#"]', { offset: 50 });
	});
	return <Router />;
}

export default App;
