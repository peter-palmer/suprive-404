import React from 'react';
import { Container } from 'react-bootstrap';
import CountUp from 'react-countup';

function Home() {
	return (
		<Container className='text-center py-5'>
			<h1 className='display-3' data-aos='fade-up'>
				<i class='las la-angle-left'></i> Welcome to Suprive <CountUp end={404} /> <i class='las la-angle-right'></i>
			</h1>
		</Container>
	);
}

export default Home;
